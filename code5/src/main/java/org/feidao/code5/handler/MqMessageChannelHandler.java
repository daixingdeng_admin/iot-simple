package org.feidao.code5.handler;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import lombok.extern.slf4j.Slf4j;
import org.feidao.code5.rabbitMq.MqMessage;
import org.feidao.code5.rabbitMq.ProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class MqMessageChannelHandler extends ChannelInboundHandlerAdapter {
    @Autowired
    ProducerService producerService;

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        if (!(msg instanceof MqMessage)) {
            return;
        }
        MqMessage mqMessage = (MqMessage) msg;
        log.info("转发到Rabbitmq Server：" + mqMessage.data);
        producerService.sendData(mqMessage.data);
    }

}
