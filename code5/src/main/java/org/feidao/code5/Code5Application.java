package org.feidao.code5;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created with IntelliJ IDEA.
 * User: linghufeixia
 * Date: 2022-11-16
 * Description: Application
 */
@SpringBootApplication
public class Code5Application {

	public static void main(String[] args) {
		SpringApplication.run(Code5Application.class, args);
	}
}
