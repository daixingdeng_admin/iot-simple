package org.feidao.code5.netty;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created with IntelliJ IDEA.
 * User: linghufeixia
 * Date: 2022-11-16
 * Description: mqtt broker的启动
 */

@Component
public class TCPServerStartListener implements ApplicationRunner {
    @Resource
    TCPServer TCPServer;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        TCPServer.start();
    }
}
