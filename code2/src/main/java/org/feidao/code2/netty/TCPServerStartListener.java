package org.feidao.code2.netty;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created with IntelliJ IDEA.
 * User: linghufeixia
 * Date: 2022-11-12
 * Description: 服务端的启动
 */

@Component
public class TCPServerStartListener implements ApplicationRunner {
    @Resource
    TCPServer tcpServer;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        tcpServer.start();
    }
}
