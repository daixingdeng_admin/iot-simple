package org.feidao.code2.netty;


import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.handler.timeout.IdleStateHandler;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA.
 * User: linghufeixia
 * Date: 2022-11-12
 * Description: server channel初始化器
 */
@Component
public class TCPServerChannelInitializer extends ChannelInitializer {

    @Resource
    TCPServerHandler tcpServerHandler;

    protected void initChannel(Channel channel) throws Exception {
        //心跳超时控制
        channel.pipeline().addLast("idle",
                new IdleStateHandler(15, 0, 0, TimeUnit.MINUTES));
        //自己的业务
        channel.pipeline().addLast(tcpServerHandler);
    }
}
