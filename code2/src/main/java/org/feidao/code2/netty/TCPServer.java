package org.feidao.code2.netty;


import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created with IntelliJ IDEA.
 * User: linghufeixia
 * Date: 2022-11-12
 * Description: 服务端
 */
@Slf4j
@Component
public class TCPServer {
    @Value("${netty.port:8082}")
    private int port;
    @Value("${netty.bossThreadNum:1}")
    private int bossThreadNum;

    @Resource
    private TCPServerChannelInitializer TCPServerChannelInitializer;

    // 多线程事件循环器:接收的连接
    private EventLoopGroup bossGroup;
    // 实际工作的线程组 多线程事件循环器:处理已经被接收的连接
    private EventLoopGroup workGroup;
    private ChannelFuture channelFuture;

    public TCPServer() {
    }

    public void start() {
        log.info("TCPServer start:" + port);
        bossGroup = new NioEventLoopGroup(this.bossThreadNum);
        workGroup = new NioEventLoopGroup();

        try {
            ServerBootstrap serverBootstrap = new ServerBootstrap();

            serverBootstrap.group(bossGroup, workGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(TCPServerChannelInitializer)
                    .option(ChannelOption.SO_BACKLOG, 128)
                    .childOption(ChannelOption.SO_KEEPALIVE, true);

            // 绑定端口，开始接收进来的连接
            channelFuture = serverBootstrap.bind(port).sync();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            destroy();
        }
    }

    public void destroy() {
        // 关闭socket
        try {
            channelFuture.channel().closeFuture().sync();
        } catch (Exception e) {
            e.printStackTrace();
        }

        bossGroup.shutdownGracefully();
        workGroup.shutdownGracefully();
        bossGroup = null;
        workGroup = null;
    }
}
