package org.feidao.code2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Code2Application {

	public static void main(String[] args) {
		SpringApplication.run(Code2Application.class, args);
	}
}
