package com.feidao.code1.client;

import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;

/**
 * Created with IntelliJ IDEA.
 * User: feidao
 * Date: 2021-4-9
 * Description: client Channel初始化器
 */
public class TestClientChannelInitializer extends ChannelInitializer {
    protected void initChannel(Channel channel) throws Exception {
        channel.pipeline().addLast(new TestClientHandler());
    }
}
