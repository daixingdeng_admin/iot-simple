package com.feidao.code1.client;

/**
 * Created with IntelliJ IDEA.
 * User: feidao
 * Date: 2021-4-7
 * Description: 启动client
 */
public class ClientStarter {
    public static void main(String[] args) throws Exception {
        System.out.println("client start...");
        String ip = "127.0.0.1";
        int port = 8081;
        new TestClient(ip, port).start();
    }
}
