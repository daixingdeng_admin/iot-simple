package com.feidao.code1.server;

/**
 * Created with IntelliJ IDEA.
 * User: feidao
 * Date: 2021-4-7
 * Description: 启动server
 */
public class ServerStarter {
    public static void main(String[] args) throws Exception {
        System.out.println("server start...");
        int port = 8081;
        new TestServer(port).start();
    }
}
