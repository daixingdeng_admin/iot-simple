package com.feidao.code1.server;

import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;

/**
 * Created with IntelliJ IDEA.
 * User: feidao
 * Date: 2021-4-9
 * Description: server channel初始化器
 */
public class TestServerChannelInitializer extends ChannelInitializer {
    protected void initChannel(Channel channel) throws Exception {
        channel.pipeline().addLast(new TestServerHandler());

    }
}
