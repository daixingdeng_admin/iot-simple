package org.feidao.code4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created with IntelliJ IDEA.
 * User: linghufeixia
 * Date: 2022-12-3
 * Description: Application
 */
@SpringBootApplication
public class Code4Application {

	public static void main(String[] args) {
		SpringApplication.run(Code4Application.class, args);
	}
}
