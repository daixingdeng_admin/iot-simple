package org.feidao.code4.dataProcessor;

import io.netty.channel.Channel;
import org.feidao.code4.message.MBAPHeader;
import org.feidao.code4.message.PduPayload;

/**
 * Created with IntelliJ IDEA.
 * User: linghufeixia
 * Date: 2022-12-4
 * Description: 接受消息处理的策略
 */
public interface RecMessageStrategy {
    void recMessage(Channel channel, MBAPHeader mbapHeader, PduPayload pduPayload);
}
