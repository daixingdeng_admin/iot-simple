package org.feidao.code4.dataProcessor.res;

import io.netty.channel.Channel;
import lombok.extern.slf4j.Slf4j;
import org.feidao.code4.common.ByteUtil;
import org.feidao.code4.dataProcessor.RecMessageStrategy;
import org.feidao.code4.message.MBAPHeader;
import org.feidao.code4.message.PduPayload;

@Slf4j
public class WriteMultipleCoilsResMessageStrategy implements RecMessageStrategy {
    @Override
    public void recMessage(Channel channel, MBAPHeader mbapHeader, PduPayload pduPayload) {
        log.info("-----------recMessage:写多线圈 begin------------");
        log.info("header: " + mbapHeader.toString());
        log.info("functionCode: " + pduPayload.getFunctionCode());
        log.info("data hex data: " + ByteUtil.bytesToHexString(pduPayload.getData()));
        log.info("-----------recMessage:写多线圈 end------------");
    }
}
