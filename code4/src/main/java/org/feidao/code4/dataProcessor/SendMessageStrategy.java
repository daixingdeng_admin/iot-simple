package org.feidao.code4.dataProcessor;

import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;
import org.feidao.code4.message.PduPayload;

/**
 * Created with IntelliJ IDEA.
 * User: linghufeixia
 * Date: 2022-12-4
 * Description: Modbus TCP 发送消息策略
 */
@Slf4j
public class SendMessageStrategy {

    public ByteBuf sendMessage(ByteBuf byteBuf, PduPayload pduPayload) {
        log.info("SendMessageStrategy ");
        byteBuf.writeByte(pduPayload.getFunctionCode());
        byteBuf.writeBytes(pduPayload.getData());
        return byteBuf;
    }
}
