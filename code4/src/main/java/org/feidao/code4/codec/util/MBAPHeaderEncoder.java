package org.feidao.code4.codec.util;

import io.netty.buffer.ByteBuf;
import org.feidao.code4.message.MBAPHeader;

/**
 * Created with IntelliJ IDEA.
 * User: linghufeixia
 * Date: 2022-12-3
 * Description: Modbus TCP header编码
 */
public class MBAPHeaderEncoder {
    public static void encode(ByteBuf byteBuf, MBAPHeader mbapHeader) {
        byteBuf.writeShort(mbapHeader.getTransactionId());
        byteBuf.writeShort(mbapHeader.getProtocolId());
        byteBuf.writeShort(mbapHeader.getLength());
        byteBuf.writeByte(mbapHeader.getUnitId());
    }
}
