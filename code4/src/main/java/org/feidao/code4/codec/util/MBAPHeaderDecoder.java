package org.feidao.code4.codec.util;

import io.netty.buffer.ByteBuf;
import org.feidao.code4.message.MBAPHeader;

/**
 * Created with IntelliJ IDEA.
 * User: linghufeixia
 * Date: 2022-12-3
 * Description: Modbus TCP MBAP Header解码
 */
public class MBAPHeaderDecoder {

    public static MBAPHeader decode(ByteBuf byteBuf) {
        MBAPHeader mbapHeader = new MBAPHeader();
        short transactionId = byteBuf.readShort();
        mbapHeader.setTransactionId(transactionId);
        short protocolId = (short) byteBuf.readShort();
        mbapHeader.setProtocolId(protocolId);
        short length = (short) byteBuf.readUnsignedShort();
        mbapHeader.setLength(length);
        byte unitId = (byte) byteBuf.readUnsignedByte();
        mbapHeader.setUnitId(unitId);
        return mbapHeader;
    }
}
