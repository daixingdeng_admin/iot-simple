package org.feidao.code4.message;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.DefaultByteBufHolder;

/**
 * Created with IntelliJ IDEA.
 * User: linghufeixia
 * Date: 2022-12-1
 * Description: 用于pdu解码
 */
public class TCPModbusByteBufHolder extends DefaultByteBufHolder{
    public int functionCode;

    public TCPModbusByteBufHolder(ByteBuf data) {
        super(data) ;
    }

    public int getFunctionCode() {
        return functionCode;
    }

    public void setFunctionCode(int functionCode) {
        this.functionCode = functionCode;
    }
}
