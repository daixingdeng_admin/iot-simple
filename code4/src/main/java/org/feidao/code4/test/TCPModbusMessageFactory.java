package org.feidao.code4.test;

import org.feidao.code4.common.ByteUtil;
import org.feidao.code4.common.FunctionCodeConstants;
import org.feidao.code4.common.ModbusConstants;
import org.feidao.code4.message.MBAPHeader;
import org.feidao.code4.message.PduPayload;

/**
 * Created with IntelliJ IDEA.
 * User: linghufeixia
 * Date: 2022-12-3
 * Description: Modbus TCP 消息 Factory，用于测试
 */
public class TCPModbusMessageFactory {

    //构建 00 01 00 00 00 06 01...
    public static MBAPHeader newReadCoilsReqHeader(){
        byte uuid = 1;
        short transactionId = 1;
        short length = 6; // uuid(1) + code(1) + start(2) + num(2)
        MBAPHeader mbapHeader = new MBAPHeader();
        mbapHeader.setTransactionId(transactionId);
        mbapHeader.setProtocolId(ModbusConstants.PROTOCOLID);
        mbapHeader.setUnitId(uuid);
        mbapHeader.setLength(length);
        return mbapHeader;
    }

    public static PduPayload newReadCoilsReqPdu() {
        PduPayload  pduPayload = new PduPayload();

        String address = "00AC";//寄存器起始地址
        int quantity = 1;//寄存器的数量
        short bytelength = 4;
        byte[] pduBytes = new byte[4];
        //pdu
        pduPayload.setFunctionCode(FunctionCodeConstants.ReadCoils);
        pduPayload.setDataLength(bytelength);
        ByteUtil.copyBytes(pduBytes,ByteUtil.hexStringToBytes(address),0);
        ByteUtil.copyBytes(pduBytes,ByteUtil.intToBytesBig(quantity),2);
        pduPayload.setData(pduBytes);
        return pduPayload;
    }

    //构建 00 02 00 00 00 06 01...
    public static MBAPHeader newReadHoldingRegistersReqHeader(){
        byte uuid = 1;
        short transactionId = 2;
        short length = 6; // uuid(1) + code(1) + start(2) + num(2)
        MBAPHeader mbapHeader = new MBAPHeader();
        mbapHeader.setTransactionId(transactionId);
        mbapHeader.setProtocolId(ModbusConstants.PROTOCOLID);
        mbapHeader.setUnitId(uuid);
        mbapHeader.setLength(length);
        return mbapHeader;
    }

    public static PduPayload newReadHoldingRegistersReqPdu() {
        PduPayload pduPayload = new PduPayload();
        String address = "00AC";//寄存器起始地址
        int quantity = 2;//寄存器的数量
        short bytelength = 4;
        byte[] pduBytes = new byte[4];
        pduPayload.setFunctionCode(FunctionCodeConstants.ReadHoldingRegisters);
        pduPayload.setDataLength(bytelength);
        ByteUtil.copyBytes(pduBytes, ByteUtil.hexStringToBytes(address),0);
        ByteUtil.copyBytes(pduBytes,ByteUtil.intToBytesBig(quantity),2);
        pduPayload.setData(pduBytes);
        return pduPayload;
    }

    //构建 00 03 00 00 00 06 01...
    public static MBAPHeader newWriteSingleCoilReqHeader(){
        byte uuid = 1;
        short transactionId = 3;
        short length = 6; // uuid(1) + code(1) + start(2) + num(2)
        MBAPHeader mbapHeader = new MBAPHeader();
        mbapHeader.setTransactionId(transactionId);
        mbapHeader.setProtocolId(ModbusConstants.PROTOCOLID);
        mbapHeader.setUnitId(uuid);
        mbapHeader.setLength(length);
        return mbapHeader;
    }

    public static PduPayload newWriteSingleCoilReqPdu() {
        PduPayload pduPayload = new PduPayload();
        String address = "00AC";//寄存器起始地址
        int values = 1;//写入的数值
        short bytelength = 4;
        byte[] pduBytes = new byte[4];
        pduPayload.setFunctionCode(FunctionCodeConstants.WriteSingleCoil);
        pduPayload.setDataLength(bytelength);
        ByteUtil.copyBytes(pduBytes,ByteUtil.hexStringToBytes(address),0);
        ByteUtil.copyBytes(pduBytes,ByteUtil.intToBytesBig(values),2);

        pduPayload.setData(pduBytes);
        return pduPayload;
    }

    //构建 00 04 00 00 00 06 01...
    public static MBAPHeader newWriteSingleRegisterReqHeader(){
        byte uuid = 1;
        short transactionId = 4;
        short length = 6; // uuid(1) + code(1) + start(2) + num(2)
        MBAPHeader mbapHeader = new MBAPHeader();
        mbapHeader.setTransactionId(transactionId);
        mbapHeader.setProtocolId(ModbusConstants.PROTOCOLID);
        mbapHeader.setUnitId(uuid);
        mbapHeader.setLength(length);
        return mbapHeader;
    }

    public static PduPayload newWriteSingleRegisterReqPdu() {
        PduPayload pduPayload = new PduPayload();
        String address = "00AC";//寄存器起始地址
        int values = 27;//写入的数值
        short bytelength = 4;
        byte[] pduBytes = new byte[4];
        pduPayload.setFunctionCode(FunctionCodeConstants.WriteSingleRegister);
        pduPayload.setDataLength(bytelength);
        ByteUtil.copyBytes(pduBytes,ByteUtil.hexStringToBytes(address),0);
        ByteUtil.copyBytes(pduBytes,ByteUtil.intToBytesBig(27),2);

        pduPayload.setData(pduBytes);
        return pduPayload;
    }
}
