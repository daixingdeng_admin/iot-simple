package org.feidao.code4.netty;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created with IntelliJ IDEA.
 * User: linghufeixia
 * Date: 2022-11-15
 * Description: mqtt订阅者的启动
 */

@Component
public class TCPServerStartListener implements ApplicationRunner {
    @Resource
    TCPServer tcpserver;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        tcpserver.start();
    }
}
