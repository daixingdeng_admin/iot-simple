package org.feidao.code4.netty;

import io.netty.channel.Channel;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created with IntelliJ IDEA.
 * User: linghufeixia
 * Date: 2022-12-7
 * Description: channel容器
 */
public class ChannelManager {

    /**
     * 全局map，保存通道
     */
    private static final ConcurrentHashMap<String, Channel> channelMap = new ConcurrentHashMap<>(128);

    public static ConcurrentHashMap<String, Channel> getChannelMap() {
        return channelMap;
    }

    /**
     *  获取channel
     */
    public static Channel getChannel(String channelId){
        if(CollectionUtils.isEmpty(channelMap)){
            return null;
        }
        return channelMap.get(channelId);
    }


    /**
     *  添加channel
     */
    public static void addChannel(String channelId,Channel channel){
        channelMap.put(channelId,channel);
    }

    /**
     *  移除channel
     */
    public static boolean removeChannel(String channelId){
        if(channelMap.containsKey(channelId)){
            channelMap.remove(channelId);
            return true;
        }
        return false;
    }

    /**
     * 返回channel列表
     * @return
     */
    public static List<String> list(){
        List<String> list = new ArrayList<>();
        if(CollectionUtils.isEmpty(channelMap)){
            return null;
        }
        for (Map.Entry<String, Channel> iterator : channelMap.entrySet()) {
            String channelId = iterator.getKey();
            Channel channel = iterator.getValue();
            list.add(channelId);
        }
        return list;

    }
}
