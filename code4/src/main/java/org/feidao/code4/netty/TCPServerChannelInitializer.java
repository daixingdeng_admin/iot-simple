package org.feidao.code4.netty;


import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import org.feidao.code4.handler.TCPModbusReqEncoder;
import org.feidao.code4.handler.TCPModbusResDecoder;
import org.feidao.code4.handler.TCPModbusResHandler;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * User: linghufeixia
 * Date: 2022-12-1
 * Description: channel初始化器
 */
@Component
public class TCPServerChannelInitializer extends ChannelInitializer<SocketChannel> {

    public TCPServerChannelInitializer() {
        super();
    }

    @Override
    protected void initChannel(SocketChannel socketChannel) throws Exception {
        socketChannel.pipeline().addLast("decoder", new TCPModbusResDecoder());
        socketChannel.pipeline().addLast("encoder", new TCPModbusReqEncoder());
        socketChannel.pipeline().addLast("tcpModbus", new TCPModbusResHandler());
    }
}
