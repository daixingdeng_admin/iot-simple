package org.feidao.code4.controller;


import io.netty.channel.Channel;
import org.feidao.code4.message.MBAPHeader;
import org.feidao.code4.message.PduPayload;
import org.feidao.code4.message.TCPModbusMessage;
import org.feidao.code4.netty.ChannelManager;
import org.feidao.code4.test.TCPModbusMessageFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: linghufeixia
 * Date: 2022-12-4
 * Description:
 */
@RestController
@RequestMapping(value = "/modbus")
public class ModbusController {

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String list() {
        List<String> list = ChannelManager.list();
        return list.toString();

    }

    //读取线圈状态
    @RequestMapping(value = "/readCoils", method = RequestMethod.GET)
    public String readCoils(String ip) {
        Channel channel = ChannelManager.getChannel(ip);
        if (channel != null) {
            MBAPHeader mbapHeader = TCPModbusMessageFactory.newReadCoilsReqHeader();
            PduPayload pduPayload = TCPModbusMessageFactory.newReadCoilsReqPdu();

            TCPModbusMessage tcpModbusMessage = new TCPModbusMessage();
            tcpModbusMessage.mbapHeader = mbapHeader;
            tcpModbusMessage.pduPayload = pduPayload;
            channel.writeAndFlush(tcpModbusMessage);
            return "sucess";
        } else {
            return "fail";
        }
    }

    //读取保持寄存器
    @RequestMapping(value = "/readHoldingRegisters", method = RequestMethod.GET)
    public String readHoldingRegisters(@RequestParam(value = "ip") String ip) {
        Channel channel = ChannelManager.getChannel(ip);
        if (channel != null) {
            MBAPHeader mbapHeader = TCPModbusMessageFactory.newReadHoldingRegistersReqHeader();
            PduPayload pduPayload = TCPModbusMessageFactory.newReadHoldingRegistersReqPdu();

            TCPModbusMessage tcpModbusMessage = new TCPModbusMessage();
            tcpModbusMessage.mbapHeader = mbapHeader;
            tcpModbusMessage.pduPayload = pduPayload;
            channel.writeAndFlush(tcpModbusMessage);
            return "sucess";
        } else {
            return "fail";
        }
    }

    //写单个线圈
    @RequestMapping(value = "/writeSingleCoil", method = RequestMethod.GET)
    public String writeSingleCoil(@RequestParam(value = "ip") String ip) {
        Channel channel = ChannelManager.getChannel(ip);
        if (channel != null) {
            MBAPHeader mbapHeader = TCPModbusMessageFactory.newWriteSingleCoilReqHeader();
            PduPayload pduPayload = TCPModbusMessageFactory.newWriteSingleCoilReqPdu();

            TCPModbusMessage tcpModbusMessage = new TCPModbusMessage();
            tcpModbusMessage.mbapHeader = mbapHeader;
            tcpModbusMessage.pduPayload = pduPayload;
            channel.writeAndFlush(tcpModbusMessage);
            return "sucess";
        } else {
            return "fail";
        }
    }

    //写单个寄存器
    @RequestMapping(value = "/writeSingleRegister", method = RequestMethod.GET)
    public String writeSingleRegister(@RequestParam(value = "ip") String ip) {
        Channel channel = ChannelManager.getChannel(ip);
        if (channel != null) {
            MBAPHeader mbapHeader = TCPModbusMessageFactory.newWriteSingleRegisterReqHeader();
            PduPayload pduPayload = TCPModbusMessageFactory.newWriteSingleRegisterReqPdu();

            TCPModbusMessage tcpModbusMessage = new TCPModbusMessage();
            tcpModbusMessage.mbapHeader = mbapHeader;
            tcpModbusMessage.pduPayload = pduPayload;
            channel.writeAndFlush(tcpModbusMessage);
            return "sucess";
        } else {
            return "fail";
        }
    }
}
